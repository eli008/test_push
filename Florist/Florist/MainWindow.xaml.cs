﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Florist
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //st.db.Product.ToList().ForEach(q => {
            //var directory = Directory.GetFiles(@"D:\Users\Оля\Desktop\демка=хуемка\09_1.6-2022_2\Вариант 2\Вариант 2\Сессия 1\Товар_import");

            // if (directory.FirstOrDefault(w => w.Contains(q.ProductArticleNumber)) != null)
            // {
            //     q.ProductPhotoBitmap = File.ReadAllBytes(directory.FirstOrDefault(w => w.Contains(q.ProductArticleNumber)));
            //        st.db.SaveChanges();
            //    }                

            //});
        }

        private void BtnAuth_Click(object sender, RoutedEventArgs e)
        {
            var login = txtLog.Text;
            var pass = txtPass.Password;

            User user;

            if ((user = st.db.User.FirstOrDefault(q => q.UserLogin == login && q.UserPassword == pass)) != null)
            {
                Hide();
                new ProductsWindow(user).ShowDialog();
                Show();

            }
            else
            {
                MessageBox.Show("Неправильный логин или пароль");
            }
        }

        private void BtnGuest_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            new ProductsWindow(null).ShowDialog();
            Show();

        }
    }
}
